package model;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;

/**
 * Created by Darya_Sliunkova on 4/22/2018.
 */
public class UserTest {
    protected DataSource dataSource;

    @Before
    public void init() throws Exception {
        DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
    }

    @After
    public void after() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
    }

    private IDatabaseConnection getConnection() throws Exception {
        final Connection con = dataSource.getConnection();
        final DatabaseMetaData databaseMetaData = con.getMetaData();
        final IDatabaseConnection connection = new DatabaseConnection(con,
                databaseMetaData.getUserName().toUpperCase());
        final DatabaseConfig config = connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new Oracle10DataTypeFactory());
        config.setProperty(
                DatabaseConfig.FEATURE_SKIP_ORACLE_RECYCLEBIN_TABLES,
                Boolean.TRUE);
        return connection;
    }

    protected IDataSet getDataSet() throws Exception {
        final File file = new File("src/test/resources/test-db.xml");
        return new FlatXmlDataSetBuilder().build(file);
    }
}
