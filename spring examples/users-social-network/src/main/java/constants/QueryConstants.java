package constants;

/**
 * Created by Darya_Sliunkova on 4/19/2018.
 */
public class QueryConstants {
    public static final String DELETE_USERS = "DROP TABLE Users IF EXISTS";
    public static final String DELETE_FRIENDSHIPS = "DROP TABLE FRIENDSHIPS IF EXISTS ";
    public static final String DELETE_POSTS = "DROP TABLE POSTS IF EXISTS";
    public static final String DELETE_LIKES = "DROP TABLE LIKES IF EXISTS";

    public static final String CREATE_USERS = "CREATE TABLE  IF NOT EXISTS Users (id INT not NULL AUTO_INCREMENT, name VARCHAR(255), surname VARCHAR(255), birthday date, PRIMARY KEY(id))";
    public static final String CREATE_FRIENDSHIPS = "CREATE TABLE  IF NOT EXISTS Friendships (id_user1 INT not NULL, id_user2 INT not NULL, date TIMESTAMP default CURRENT_TIMESTAMP)";
    public static final String CREATE_POSTS = "CREATE TABLE  IF NOT EXISTS Posts (id INT not NULL AUTO_INCREMENT, id_user INT not NULL, text VARCHAR(255), current timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(id))";
    public static final String CREATE_LIKES = "CREATE TABLE  IF NOT EXISTS Likes (id_user INT not NULL, id_post INT not NULL, current timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP)";

    public static  String INSERT_USER = "INSERT INTO Users (name, surname, birthday) VALUES ( ?, ?, ?)";
    public static final String SELECT_USERS = "SELECT name FROM Users ORDER BY name";

    public static final String INSERT_FRIENDSHIP = "INSERT INTO Friendships (id_user1, id_user2, date) VALUES( ?, ?, ?)";

    public static final String INSERT_POSTS = "INSERT INTO Posts (id_user, text, current) VALUES(?,?,?)";
    public static final String INSERT_LIKES = "INSERT INTO Likes (id_user, id_post, current) VALUES(?,?,?)";

    public static final String SELECT_TOP_USERS = "select friends.id, name, count(*) from (select id , name, count(*) FROM users as u1, Friendships WHERE u1.id = Friendships.id_user1 or u1.id = Friendships.id_user2 GROUP BY ID, NAME HAVING count(*) > 100 order by name) as friends, posts, likes where friends.id = posts.id_user and posts.id = likes.id_post and likes.current >= TO_TIMESTAMP( '2018-03-01', 'yyyy-mm-dd' ) AND likes.current < TO_TIMESTAMP( '2018-03-31', 'yyyy-mm-dd') group by friends.id, name having count(*) > 100 ";
}
