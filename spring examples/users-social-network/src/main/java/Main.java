import model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.DBService;

import java.io.IOException;
import java.text.ParseException;

/**
 * Created by Darya_Sliunkova on 4/19/2018.
 */
public class Main {
    public static void main(String[] args) {
        ApplicationContext ct = new ClassPathXmlApplicationContext("beans.xml");
        DBService dbService = (DBService) ct.getBean("dbService");

        dbService.dropTables();
        dbService.createTables();


        try {
            dbService.uploadUsers();
            dbService.uploadFriendships();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dbService.uploadPosts();
        dbService.uploadLikes();
        for (User user : dbService.selectTopUsers()) {
            System.out.println(user.getName());
        }

    }
}
