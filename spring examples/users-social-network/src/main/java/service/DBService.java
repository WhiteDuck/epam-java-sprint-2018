package service;

import constants.QueryConstants;
import model.User;
import org.apache.commons.io.FileUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Darya_Sliunkova on 4/19/2018.
 */
public class DBService {
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate simpleJdbcInsert;

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.simpleJdbcInsert = new NamedParameterJdbcTemplate(dataSource);
    }

    public void dropTables() {
        jdbcTemplate.execute(QueryConstants.DELETE_USERS);
        jdbcTemplate.execute(QueryConstants.DELETE_FRIENDSHIPS);
        jdbcTemplate.execute(QueryConstants.DELETE_POSTS);
        jdbcTemplate.execute(QueryConstants.DELETE_LIKES);
    }

    public void createTables() {
        jdbcTemplate.execute(QueryConstants.CREATE_USERS);
        jdbcTemplate.execute(QueryConstants.CREATE_FRIENDSHIPS);
        jdbcTemplate.execute(QueryConstants.CREATE_POSTS);
        jdbcTemplate.execute(QueryConstants.CREATE_LIKES);
    }

    public void uploadUsers() throws IOException, ParseException {
        List<String> lines = getLinesFromFile("src/main/resources/users.txt");

        jdbcTemplate.batchUpdate(QueryConstants.INSERT_USER, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                String line = lines.get(i);
                String[] elements = line.split(";");
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = null;
                try {
                    date = new Date(formatter.parse(elements[2]).getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                preparedStatement.setString(1, elements[0]);
                preparedStatement.setString(2, elements[1]);
                preparedStatement.setDate(3, date);
            }

            @Override
            public int getBatchSize() {
                return lines.size();
            }
        });
    }

    private List<String> getLinesFromFile(String filename) throws IOException {
        return FileUtils.readLines(Paths.get(filename).toFile(), "UTF-8");
    }

    public void uploadFriendships() throws IOException {
        List<String> lines = getLinesFromFile("src/main/resources/friendships.txt");

        jdbcTemplate.batchUpdate(QueryConstants.INSERT_FRIENDSHIP, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                String line = lines.get(i);
                String[] elements = line.split(";");
                preparedStatement.setInt(1, Randomizer.generateUserId());
                preparedStatement.setInt(2, Randomizer.generateUserId());
                preparedStatement.setTimestamp(3, Randomizer.generateTimestamp());
            }

            @Override
            public int getBatchSize() {
                return lines.size();
            }
        });

    }

    public void uploadPosts() {
        int i = 1;
        final int batchSize = 300;
        while (i < batchSize) {
            jdbcTemplate.batchUpdate(QueryConstants.INSERT_POSTS, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                    preparedStatement.setInt(1, Randomizer.generateUserId());
                    preparedStatement.setString(2, Randomizer.generateString());
                    preparedStatement.setTimestamp(3, Randomizer.generateTimestamp());
                }

                @Override
                public int getBatchSize() {
                    return batchSize;
                }
            });
            i++;
        }
    }

    public void uploadLikes() {
        int i = 1;
        int batchSize = 300000;
        while (i < batchSize) {
            jdbcTemplate.batchUpdate(QueryConstants.INSERT_LIKES, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                    preparedStatement.setInt(1, Randomizer.generateUserId());
                    preparedStatement.setInt(2, Randomizer.generatePostId());
                    preparedStatement.setTimestamp(3, Randomizer.generateTimestamp());
                }

                @Override
                public int getBatchSize() {
                    return batchSize;
                }
            });
            i++;
        }
    }

    public List<User> selectTopUsers() {
        List<Map<String, Object>> list = jdbcTemplate.queryForList(QueryConstants.SELECT_TOP_USERS);
        List<User> users = new ArrayList<>();
        for (Map<String, Object> row : list) {
            users.add(new User(row.get("name").toString()));

        }
        return users;
    }
}
