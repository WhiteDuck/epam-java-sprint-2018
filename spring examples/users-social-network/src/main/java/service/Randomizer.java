package service;

import org.apache.commons.lang3.RandomStringUtils;

import java.sql.Timestamp;
import java.util.Random;

/**
 * Created by Darya_Sliunkova on 4/21/2018.
 */
public class Randomizer {
    public static Timestamp generateTimestamp() {
        long offset = Timestamp.valueOf("2018-01-01 00:00:00").getTime();
        long end = Timestamp.valueOf("2018-12-31 23:59:00").getTime();
        long diff = end - offset + 1;
        return new Timestamp(offset + (long) (Math.random() * diff));
    }

    public static String generateString() {
        int length = 10;
        boolean useLetters = true;
        boolean useNumbers = false;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }

    public static int generatePostId() {
        Random rn = new Random();
        return rn.nextInt(300) + 1;
    }

    public static int generateUserId() {
        Random rn = new Random();
        return rn.nextInt(1099) + 1;
    }


}
