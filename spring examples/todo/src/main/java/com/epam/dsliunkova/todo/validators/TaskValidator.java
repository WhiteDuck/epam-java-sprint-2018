package com.epam.dsliunkova.todo.validators;

import com.epam.dsliunkova.todo.model.Task;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by Darya_Sliunkova on 4/10/2018.
 */
@Component
public class TaskValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Task.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        Task task = (Task) o;
        if (task.getTitle().isEmpty()){
            errors.rejectValue("title", "title.empty");
        }
    }
}
