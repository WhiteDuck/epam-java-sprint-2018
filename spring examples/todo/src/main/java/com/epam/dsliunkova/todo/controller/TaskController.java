package com.epam.dsliunkova.todo.controller;

import com.epam.dsliunkova.todo.model.Task;
import com.epam.dsliunkova.todo.services.TaskService;
import com.epam.dsliunkova.todo.validators.TaskValidator;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.Calendar;


/**
 * Created by Darya_Sliunkova on 3/30/2018.
 */
@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping("/tasks")
public class TaskController {
    private TaskService taskService;
    private TaskValidator taskValidator;

    @Autowired
    public TaskController(TaskValidator taskValidator, TaskService taskService) {
        this.taskService = taskService;
        this.taskValidator = taskValidator;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(taskValidator);
    }

    @RequestMapping(value = "/")
    public ModelAndView getTasks() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("previousTasks", taskService.getPreviousTasks(new Date(Calendar.getInstance().getTime().getTime())));
        modelAndView.addObject("currentTasks", taskService.getCurrentTasks(new Date(Calendar.getInstance().getTime().getTime())));
        modelAndView.addObject("futureTasks", taskService.getFutureTasks(new Date(Calendar.getInstance().getTime().getTime())));
        modelAndView.setViewName("tasks");
        return modelAndView;
    }

    @RequestMapping(value = "/addTask")
    public String addTask(@Validated @ModelAttribute("task") Task task, BindingResult bindingResult, Model model, @RequestParam(value = "image", required = false) Part file) throws Exception {
        if (bindingResult.hasErrors()) {
            throw new Exception("Input error");
        }

        loadImage(task, file);
        taskService.addTask(task);
        return "redirect:/tasks/";
    }


    @RequestMapping(value = "/deleteTask/{id}")
    public String addTask(@PathVariable String id) {
        taskService.deleteById(Integer.parseInt(id));
        return "redirect:/tasks/";
    }


    @RequestMapping(value = "/redirectToAddTask")
    public String redirectToAddTask(Model model) {
        model.addAttribute("task", new Task());
        return "addTask";
    }

    @RequestMapping(value = "/editStatus/{id}")
    public String editStatus(@PathVariable String id) {
        if (taskService.getById(Integer.parseInt(id)).isStatus()) {
            taskService.updateStatus(false, Integer.parseInt(id));
        } else {
            taskService.updateStatus(true, Integer.parseInt(id));
        }
        return "redirect:/tasks/";
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET, params = "id")
    public ModelAndView editTask(@RequestParam String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("updateTask");
        modelAndView.addObject("task", taskService.getById(Integer.parseInt(id)));
        return modelAndView;
    }

    @RequestMapping(value = "/updateTask", method = RequestMethod.POST)
    public String updateTask(@Validated @ModelAttribute("task") Task task, BindingResult bindingResult, Model model, @RequestParam(value = "image", required = false) Part file) throws Exception {
        if (bindingResult.hasErrors()) {
            throw new Exception("Input error");
        }
        loadImage(task, file);
        taskService.updateEntry(task);
        return "redirect:/tasks/";
    }

    @RequestMapping(value = "/locale", method = RequestMethod.GET)
    public String getTasksPage() {
        return "redirect:/tasks/";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/downloadImage/{id}", method = RequestMethod.GET)
    @ResponseBody
    public byte[] downloadImage(@PathVariable("id") Integer id) {
        return taskService.getById(id).getImage();
    }


    private void loadImage(@ModelAttribute("task") Task task,
                           @RequestParam(value = "image", required = false) Part file) throws IOException {
        byte[] content = null;
        if (file != null) {
            InputStream is = file.getInputStream();
            if (is != null) {
                content = IOUtils.toByteArray(is);
                task.setImage(content);
            }
        }
    }
}
