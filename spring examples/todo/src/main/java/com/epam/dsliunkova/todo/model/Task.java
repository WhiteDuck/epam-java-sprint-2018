package com.epam.dsliunkova.todo.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.sql.Date;


/**
 * Created by Darya_Sliunkova on 3/30/2018.
 */
@Entity
@Table(name = "task")
@Component
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "title")
    private String title;
    @Column(name = "details")
    private String details;
    @Column(name = "date")
    private Date date;
    @Column(name = "status")
    private boolean status = false;
    @Column(name = "image", columnDefinition="TEXT")
    private byte[] image;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Task(String title, String details, Date date, boolean status, byte[] image) {
        this.title = title;
        this.details = details;
        this.date = date;
        this.status = status;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Task() {
    }
}
