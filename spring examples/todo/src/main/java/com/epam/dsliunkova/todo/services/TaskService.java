package com.epam.dsliunkova.todo.services;

import com.epam.dsliunkova.todo.model.Task;
import com.epam.dsliunkova.todo.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.List;

/**
 * Created by Darya_Sliunkova on 4/11/2018.
 */
@Component
public class TaskService {
    private TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }


    public List<Task> getPreviousTasks(Date date) {
        return taskRepository.findByDateLessThanOrStatusTrue(date);
    }

    public List<Task> getCurrentTasks(Date date) {
        return taskRepository.findByDateAndStatusFalse(date);
    }

    public List<Task> getFutureTasks(Date date) {
        return taskRepository.findByDateGreaterThan(date);
    }

    public void addTask(Task task) {
        taskRepository.save(task);
    }

    public void deleteById(Integer integer) {
        taskRepository.deleteById(integer);
    }

    public Task getById(Integer id) {
        return taskRepository.findById(id).get();
    }

    public void updateStatus(boolean status, Integer id) {
        taskRepository.updateStatus(status, id);
    }

    public void updateEntry(Task task) {
        taskRepository.updateEntry(task.getTitle(), task.getDetails(), task.getDate(), task.isStatus(), task.getId(), task.getImage());
    }
}
