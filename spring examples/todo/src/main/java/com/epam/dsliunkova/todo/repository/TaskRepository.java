package com.epam.dsliunkova.todo.repository;

import com.epam.dsliunkova.todo.model.Task;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

/**
 * Created by Darya_Sliunkova on 3/30/2018.
 */
public interface TaskRepository extends CrudRepository<Task, Integer> {
    public List<Task> findByDateLessThanOrStatusTrue(Date date);
    public List<Task> findByDateGreaterThan(Date date);
    public List<Task> findByDateAndStatusFalse(Date date);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "update task set status = ?1 where id = ?2")
    public void updateStatus(boolean status, Integer id);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "update task set title = ?1, details = ?2, date = ?3, status = ?4, image = ?6  where id = ?5")
    public void updateEntry(String title, String details, Date date, boolean status, Integer id, byte[] image);

}
